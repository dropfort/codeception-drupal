# Codeception Drupal Module

## Usage

In your `*.suite.yml` file, add `Drupal` to your enabled modules list.

### Example configuration

This will run tests under the assumption that your Drupal installation is in a
`drupal` sub-directory.

```yaml
class_name: AcceptanceTester modules:
    enabled:
        \Codeception\Module\Drupal8\Drupal8:
            root: '../web'
            relative: yes
```

### Options

#### root
Accepts: `string` Default: `current working directory`

This defines the Drupal root in relation to the `codecept.yml` file. If this isn't passed in it defaults to the current working directory.

#### relative
Accepts: `yes` or `no` Default: `no`

This allows you to specify if the path to the drupal root is relative from the
`codeception.yml` file. Accepts `yes` or `no`.
